/**
 * 
 */
package cars;

/**
 * @author Collins
 *
 */

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@LocalBean
public class CarDAO {

	@PersistenceContext
	private EntityManager em;
	
    public List<Car> getAllCars() {
       Query query=em.createQuery("SELECT * FROM Cars");
       return query.getResultList();
    }
    
    public List<Car> getCarsByName(String name) {
        Query query=em.createQuery("SELECT * FROM Cars "+
        							"WHERE name LIKe ?1");
        query.setParameter(1, "%"+name.toUpperCase()+"%");
        return query.getResultList();
     }
    
    
    public Car getCar(int id) {
        return em.find(Car.class, id);
     }
    
    public void save(Car car) {
        em.persist(car);
     }
    
    public void update(Car car) {
        em.merge(car);
     }
    public void delete(int id){
    	em.remove(getCar(id));
    }
}
