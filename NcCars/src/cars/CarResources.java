/**
 * 
 */
package cars;

/**
 * @author Collins
 *
 */
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/cars")
@Stateful
@LocalBean

public class CarResources {

	@EJB
	private CarDAO carDao;
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findAll() {
		List<Car> cars=carDao.getAllCars();
		return Response.status(200).entity(cars).build();
	}
	
	@GET 
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/{id}")
	public Response findCarById(@PathParam("id") int id) {
		Car car=carDao.getCar(id);
		return Response.status(200).entity(car).build();
	}
	
	@GET 
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/search/{query}")
	public Response findByName(@PathParam("query") String name) {
		List<Car> cars=carDao.getCarsByName(name);
		return Response.status(200).entity(cars).build();
	}
	
	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	public Response saveCar(Car car) {
		carDao.save(car);
		return Response.status(201).entity(car).build();
	}
	
	@PUT
	@Path("/{id}")
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response updateCar(Car car) {
		carDao.update(car);
		return Response.status(200).entity(car).build();
	}
	
	@DELETE
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response deleteCar(@PathParam("id") int id) {
		carDao.delete(id);
		return Response.status(204).build();
	}
}
